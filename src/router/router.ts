import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

export const enum RouteName {
  STAFF = 'staff',
  ITINERARY = 'itinerary',
  ROSTER = 'roster',
  CLASS_SCHEDULE = 'class-schedule',
}

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: 'staff'
  },
  {
    path: '/staff',
    name: RouteName.STAFF,
    component: () => import('../views/page-staff.vue')
  },
  {
    path: '/itinerary',
    name: RouteName.ITINERARY,
    component: () => import('../views/page-itinerary.vue')
  },
  {
    path: '/roster',
    name: RouteName.ROSTER,
    component: () => import('../views/page-roster.vue')
  },
  {
    path: '/class-schedule',
    name: RouteName.CLASS_SCHEDULE,
    component: () => import('../views/page-class-schedule.vue')
  },

  {
    path: '/:pathMatch(.*)*',
    redirect: '/'
  },
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

export default router
