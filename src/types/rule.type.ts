import { CalcScoreParams, JobType, Scoreboard } from "./main.type";

/** 規則
 * 
 * 排班限制與規則。
 */
export type Rule = RuleDefault | RuleBase
  | RuleCanNotWorkMoreThanDays | RuleTryTakeMoreThanDaysOnVacation
  | RuleUnsuitableSuccessionList;

interface RuleContent {
  enable: boolean;
  name: string;
  description: string;
  calcScore(params: CalcScoreParams): Scoreboard;
}

export interface RuleDefault extends RuleContent {
  key: Exclude<RuleKey, RuleBaseKey
    | RuleKey.CAN_NOT_WORK_MORE_THAN_DAYS
    | RuleKey.TRY_TAKE_MORE_THAN_DAYS_ON_VACATION
    | RuleKey.UNSUITABLE_SUCCESSION_LIST>;
  setting?: Object;
}

type RuleBaseKey = RuleKey.EQUAL_NUMBER_OF_JOBS
  | RuleKey.TRY_TO_WORK_CONTINUOUSLY;

export interface RuleBase extends RuleContent {
  key: RuleBaseKey;
  setting: {
    /** 權重分數 */
    weightedScore: number,
  },
}

export interface RuleCanNotWorkMoreThanDays extends RuleContent {
  key: RuleKey.CAN_NOT_WORK_MORE_THAN_DAYS,
  setting: {
    /** 最大天數 */
    maxDays: number,
  },
}

export interface RuleTryTakeMoreThanDaysOnVacation extends RuleContent {
  key: RuleKey.TRY_TAKE_MORE_THAN_DAYS_ON_VACATION,
  setting: {
    /** 權重分數 */
    weightedScore: number,
    /** 指定天數 */
    days: number,
  },
}

export interface RuleUnsuitableSuccessionList extends RuleContent {
  key: RuleKey.UNSUITABLE_SUCCESSION_LIST,
  setting: {
    /** 排除種類 */
    list: {
      /** 前一班 */
      before: JobType,
      /** 後一班 */
      after: JobType,
    }[],
  },
}

export enum RuleKey {
  /** 限制員工班別 */
  RESTRICTED_STAFF_SHIFTS = 'restricted-staff-shifts',
  /** 需要擁有工作資格 */
  JOB_QUALIFICATIONS_REQUIRED = 'job-qualifications-required',
  /** 排除放假成員 */
  EXCLUDING_VACATION_MEMBERS = 'excluding-vacation-members',
  /** 平分工作數量 */
  EQUAL_NUMBER_OF_JOBS = 'equal-number-of-jobs',
  /** 工作盡量連續 */
  TRY_TO_WORK_CONTINUOUSLY = 'try-to-work-continuously',
  /** 不可連續上班超過指定天數 */
  CAN_NOT_WORK_MORE_THAN_DAYS = 'can-not-work-more-than-days',
  /** 放假盡量不要放超過指定天數 */
  TRY_TAKE_MORE_THAN_DAYS_ON_VACATION = 'try-take-more-than-days-on-vacation',
  /** 同一天內不可重複上同一個班別工作 */
  NO_DUPLICATION_WORK_ON_SHIFT_ON_DAY = 'no-duplication-work-on-shift-on-day',
  /** 不宜接班種類，指定 2 班之間不宜種類。例如 N 不接 E */
  UNSUITABLE_SUCCESSION_LIST = 'unsuitable-succession-list',
}