export interface TimestampObj {
  createdAt: number;
  editedAt?: number;
  deletedAt?: number;
}

/** 資格
 * 
 * 某些工作需要特定資格才能擔任。
 */
export interface Qualification {
  name: string;
  description: string;
}

/** 工作
 * 
 * 描述工作內容。
 */
export interface Job {
  name: string;
  codename: string;
  description: string;
  /** 需要資格 */
  requiredQualifications: Qualification[];
}

/** 班別
 * 
 * 描述工作班別，例如：白班、晚班、彈性
 */
export interface JobType {
  name: string;
  codename: string;
  description: string;
}

/** 職員
 * 
 * 職員詳細資料。
 */
export interface StaffMember {
  /** 是否參加 */
  enable: boolean;
  name: string;
  codename: string;
  description: string;
  /** 此人擁有的資格 */
  qualifications: Qualification[];
  /** 工作班別 */
  jobTypes: JobType[];
}

/** 職員休假日
 * 
 * 
 */
export interface StaffMemberHoliday {
  /** 預定放假日，格式 YYYY/MM/DD */
  date: string;
  member: StaffMember;
}

export interface CalcScoreParams {
  /** 目前得分榜 */
  scoreboard: Scoreboard;
  /** 目標職缺 */
  vacancy: Vacancy;
  /** 排班日，格式 YYYY/MM/DD */
  date: string;
  classSchedule: ClassSchedule;
  holidays: StaffMemberHoliday[];
}

/** 職缺
 * 
 * 由工作、班別、資格組成，某天需要的某個工作內容
 */
export interface Vacancy {
  job: Job | null;
  jobType: JobType | null;
  executor: StaffMember | null;

  /** 需要資格，除了工作本身的資格限制，在此也可以增加資格限制。
   * 
   * 此資料會與「工作」資格合併
   */
  requiredQualifications: Qualification[];

  remark: string;
}

/** 日行程
 * 
 * 某天包含的所有工作內容
 */
export interface DayItinerary {
  /** 日期。YYYY/MM/DD */
  date: string;
  vacancies: Vacancy[];
}

/** 班表
 * 
 * 某月內所有日行程
 */
export interface ClassSchedule {
  name: string;
  description: string;
  /** (年)月份。YYYY/MM */
  month: string;
  itineraries: DayItinerary[];
  timestamp?: TimestampObj;
}


/** 職員工作統計
 * 
 * 某職員目前負荷的工作量。
 */
export interface StaffMemberWorkload {
  member: StaffMember;

  /** 工作 */
  jobs: WorkloadItem<Job>[],

  /** 班別 */
  jobTypes: WorkloadItem<JobType>[],
}

/** 工作統計項目
 * 
 * 某個項目的工作量。
 */
export interface WorkloadItem<T> {
  /** 指定統計項目 */
  item: T;
  /** 數量 */
  quantity: number,
}

/** 推薦排名
 * 
 * 人力推薦排名
 */
export interface Rank {
  /** 推薦順序，越前面越推薦 */
  order: StaffMember[];
  /** 被排除人力 */
  exclude: StaffMember[];
}

/** 人力得分
 * 
 * 人力推薦排名
 */
export interface StaffMemberScore {
  /** 人力 */
  member: StaffMember;
  /** 得分 */
  score: number;
}

/** 人力得分榜
 * 
 * 所有人力得分
 */
export interface Scoreboard {
  /** 所有人力分數 */
  list: StaffMemberScore[];
  /** 被排除人力 */
  excludes: StaffMemberScore[];
}
