import { hasWorkedContinuously, hasContinuousVacation, jobsEq, jobTypesEq } from '@/common/utils';
import { Job, JobType, StaffMember, DayItinerary } from '@/types/main.type';

describe('jobsEq', () => {
  const jobA: Job = {
    name: '工作A',
    codename: 'A',
    description: '',
    requiredQualifications: [],
  }

  const jobB: Job = {
    name: '工作B',
    codename: 'B',
    description: '',
    requiredQualifications: [],
  }

  it('完全不相等', () => {
    const result = jobsEq(jobA, jobB);

    expect(result).toEqual(false);
  });

  it('不相等（name 不同）', () => {
    const job01: Job = {
      ...jobA,
      name: '123'
    }

    const job02: Job = {
      ...jobA,
      name: '3456'
    }

    expect(jobsEq(job01)(job02)).toEqual(false)
  });

  it('不相等（codename 不同）', () => {
    const job01: Job = {
      ...jobA,
      codename: '123'
    }

    const job02: Job = {
      ...jobA,
      codename: '3456'
    }

    expect(jobsEq(job01)(job02)).toEqual(false)
  });

  it('不相等（requiredQualifications 不同）', () => {
    const job01: Job = {
      ...jobA,
      requiredQualifications: [],
    }

    const job02: Job = {
      ...jobA,
      requiredQualifications: [{
        name: '123',
        description: '123'
      }]
    }

    expect(jobsEq(job01)(job02)).toEqual(false)
  });

  it('完全相等', () => {
    expect(jobsEq(jobA, jobA)).toEqual(true);
    expect(jobsEq(jobA)(jobA)).toEqual(true)
  });

  it('相等（description 不同）', () => {
    const job01: Job = {
      ...jobA,
      description: '123'
    }

    const job02: Job = {
      ...jobA,
      description: '3456'
    }

    expect(jobsEq(job01)(job02)).toEqual(true)
  });
});

describe('jobTypesEq', () => {
  const jobTypeA: JobType = {
    name: '班別A',
    codename: 'A',
    description: '',
  }

  const jobTypeB: JobType = {
    name: '班別B',
    codename: 'B',
    description: '',
  }

  it('完全不相等', () => {
    const result = jobTypesEq(jobTypeA, jobTypeB);

    expect(result).toEqual(false);
  });

  it('不相等（name 不同）', () => {
    const job01: JobType = {
      ...jobTypeA,
      name: '123'
    }

    const job02: JobType = {
      ...jobTypeA,
      name: '3456'
    }

    expect(jobTypesEq(job01)(job02)).toEqual(false)
  });

  it('不相等（codename 不同）', () => {
    const job01: JobType = {
      ...jobTypeA,
      codename: '123'
    }

    const job02: JobType = {
      ...jobTypeA,
      codename: '3456'
    }

    expect(jobTypesEq(job01)(job02)).toEqual(false)
  });

  it('完全相等', () => {
    expect(jobTypesEq(jobTypeA, jobTypeA)).toEqual(true);
    expect(jobTypesEq(jobTypeA)(jobTypeA)).toEqual(true)
  });

  it('相等（description 不同）', () => {
    const jobType01: JobType = {
      ...jobTypeA,
      description: '123'
    }

    const jobType02: JobType = {
      ...jobTypeA,
      description: '3456'
    }

    expect(jobTypesEq(jobType01)(jobType02)).toEqual(true)
  });
});

describe('hasWorkedContinuously', () => {
  it('已連續工作 2 天', () => {
    const executor: StaffMember = {
      enable: true,
      name: '人員1',
      codename: '',
      description: '',
      qualifications: [],
      jobTypes: [],
    }

    const itineraries: DayItinerary[] = [
      {
        date: '2021/12/01',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
      {
        date: '2021/12/02',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
    ];


    const result = hasWorkedContinuously(
      executor, '2021/12/03', itineraries, 2
    );

    expect(result).toEqual(true);
  });

  it('已連續工作 2 天後過了一天', () => {
    const executor: StaffMember = {
      enable: true,
      name: '人員1',
      codename: '',
      description: '',
      qualifications: [],
      jobTypes: [],
    }

    const itineraries: DayItinerary[] = [
      {
        date: '2021/12/01',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
      {
        date: '2021/12/02',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
    ];


    const result = hasWorkedContinuously(
      executor, '2021/12/04', itineraries, 2
    );

    expect(result).toEqual(false);
  });

  it('已連續工作 3 天', () => {
    const executor: StaffMember = {
      enable: true,
      name: '人員1',
      codename: '',
      description: '',
      qualifications: [],
      jobTypes: [],
    }

    const itineraries: DayItinerary[] = [
      {
        date: '2021/12/01',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
      {
        date: '2021/12/02',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
      {
        date: '2021/12/03',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
    ];


    const result = hasWorkedContinuously(
      executor, '2021/12/04', itineraries, 3
    );

    expect(result).toEqual(true);
  });

  it('沒有連續工作 4 天', () => {
    const executor: StaffMember = {
      enable: true,
      name: '人員1',
      codename: '',
      description: '',
      qualifications: [],
      jobTypes: [],
    }

    const itineraries: DayItinerary[] = [
      {
        date: '2021/12/01',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
      {
        date: '2021/12/02',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
      {
        date: '2021/12/03',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
    ];


    const result = hasWorkedContinuously(
      executor, '2021/12/04', itineraries, 4
    );

    expect(result).toEqual(false);
  });
});

describe('hasContinuousVacation', () => {
  it('昨天沒有放假', () => {
    const executor: StaffMember = {
      enable: true,
      name: '人員1',
      codename: '',
      description: '',
      qualifications: [],
      jobTypes: [],
    }

    const itineraries: DayItinerary[] = [
      {
        date: '2021/12/01',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
      {
        date: '2021/12/02',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
    ];


    const result = hasContinuousVacation(
      executor, '2021/12/03', itineraries, 2
    );

    expect(result).toEqual(false);
  });

  it('已連放兩天', () => {
    const executor: StaffMember = {
      enable: true,
      name: '人員1',
      codename: '',
      description: '',
      qualifications: [],
      jobTypes: [],
    }

    const itineraries: DayItinerary[] = [
      {
        date: '2021/12/01',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
    ];


    const result = hasContinuousVacation(
      executor, '2021/12/04', itineraries, 2
    );
    const inverse = hasContinuousVacation(
      executor, '2021/12/03', itineraries, 2
    );

    expect(result).toEqual(true);
    expect(inverse).toEqual(false);
  });

  it('已連放五天', () => {
    const executor: StaffMember = {
      enable: true,
      name: '人員1',
      codename: '',
      description: '',
      qualifications: [],
      jobTypes: [],
    }

    const itineraries: DayItinerary[] = [
      {
        date: '2021/12/01',
        vacancies: [
          {
            job: {
              name: '工作',
              codename: '1',
              description: '',
              requiredQualifications: [],
            },
            jobType: {
              name: '白班',
              codename: '白',
              description: '',
            },
            requiredQualifications: [],
            executor,
            remark: '',
          },
        ],
      },
    ];


    const result = hasContinuousVacation(
      executor, '2021/12/07', itineraries, 5
    );
    const inverse = hasContinuousVacation(
      executor, '2021/12/06', itineraries, 5
    );

    expect(result).toEqual(true);
    expect(inverse).toEqual(false);
  });
});
