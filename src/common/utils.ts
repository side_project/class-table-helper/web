import { Job, JobType, TimestampObj, StaffMember, DayItinerary } from '../types/main.type';
import dayjs from 'dayjs';
import { curry, isEqual } from 'lodash-es';


/** 判斷工作是否相等 */
export const jobsEq = curry((jobA: Job, jobB: Job) => {
  const { description: aD, ...a } = jobA;
  const { description: bD, ...b } = jobB;

  return isEqual(a, b);
})

/** 判斷班別是否相等 */
export const jobTypesEq = curry((jobTypeA: JobType, jobTypeB: JobType) => {
  const { description: aD, ...a } = jobTypeA;
  const { description: bD, ...b } = jobTypeB;

  return isEqual(a, b);
})

/** 建立 timestamp 物件 */
export function createTimestampObj() {
  return {
    createdAt: dayjs().unix(),
  } as TimestampObj
}

/** 是否連續工作
 * 
 * 是否連續工作超過指定天數
 * @param member 判斷成員
 * @param date 判斷日期。從這天之後是否連續指定天數。YYYY/MM/DD
 * @param itineraries 班表所有日行程
 * @param days 連續天數
 */
export function hasWorkedContinuously(
  member: StaffMember, date: string, itineraries: DayItinerary[], days: number
): boolean {
  if (days === 0) {
    return true;
  }

  // 取得前一天日行程
  const targetDate = dayjs(date).add(-1, 'd').format('YYYY/MM/DD');
  const targetItinerary = itineraries.find(itinerary => itinerary.date === targetDate);
  if (!targetItinerary) {
    return false;
  }

  const haveJob = targetItinerary.vacancies.find(({ executor }) =>
    executor?.name === member.name
  );
  if (!haveJob) {
    return false;
  }

  return hasWorkedContinuously(member, targetItinerary.date, itineraries, days - 1);
}

/** 是否連續放假
 * 
 * 是否連續放假超過指定天數
 * @param member 判斷成員
 * @param date 判斷日期。從這天之後是否連續指定天數。YYYY/MM/DD
 * @param itineraries 班表所有日行程
 * @param days 連續天數
 */
export function hasContinuousVacation(
  member: StaffMember, date: string, itineraries: DayItinerary[], days: number
): boolean {
  if (days === 0) {
    return true;
  }

  // 取得前一天日行程
  const targetDate = dayjs(date).add(-1, 'd').format('YYYY/MM/DD');
  const targetItinerary = itineraries.find(itinerary => itinerary.date === targetDate);
  if (!targetItinerary) {
    return hasContinuousVacation(member, targetDate, itineraries, days - 1);
  }

  const haveJob = targetItinerary.vacancies.find(({ executor }) =>
    executor?.name === member.name
  );
  if (haveJob) {
    return false;
  }

  return hasContinuousVacation(member, targetDate, itineraries, days - 1);
}

