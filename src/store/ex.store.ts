import { StaffMember } from '../types/main.type';
import { cloneDeep } from 'lodash-es';
import { defineStore } from 'pinia';

interface State {
  staff: StaffMember[],
}

export const useMainStore = defineStore('ex', {
  state: (): State => ({
    staff: [],
  }),
  actions: {
  },
})
