import { defineStore } from 'pinia';
import dayjs from "dayjs";
import { cloneDeep, isEqual, pick } from 'lodash-es';
import { StaffMember } from '../types/main.type';
import { Rule, RuleKey } from '../types/rule.type';
import { hasContinuousVacation, hasWorkedContinuously } from "../common/utils";

import { useStaffMemberWorkload } from '../composables/use-staff-member-workload';

interface State {
  rules: Rule[],
}

export const useRuleStore = defineStore('rule', {
  state: (): State => ({
    rules: [
      {
        key: RuleKey.RESTRICTED_STAFF_SHIFTS,
        enable: true,
        name: '限制員工班別',
        description: '人力工作班別需要含有目標工作班別，否則直接排除',
        calcScore(params) {
          const { vacancy } = params;
          const scoreboard = cloneDeep(params.scoreboard);
          scoreboard.list.length = 0;

          const staff = cloneDeep(params.scoreboard.list);

          const result = staff.reduce((board, memberScore) => {
            const { member } = memberScore;

            const eligible = member.jobTypes.find((jobType) => {
              return jobType.name === vacancy.jobType?.name;
            });

            if (eligible) {
              board.list.push(memberScore);
            } else {
              board.excludes.push(memberScore);
            }

            return board;
          }, scoreboard);

          return result;
        },
      },

      {
        key: RuleKey.JOB_QUALIFICATIONS_REQUIRED,
        enable: true,
        name: '需要擁有工作資格',
        description: '人力需要擁有職缺要求資格才可以列入候選，若無資格則直接排除',
        calcScore(params) {
          const { vacancy } = params;
          const scoreboard = cloneDeep(params.scoreboard);

          const requiredQualifications = [
            ...vacancy.requiredQualifications,
            ...vacancy.job?.requiredQualifications ?? [],
          ];

          const staff = cloneDeep(scoreboard.list);
          scoreboard.list.length = 0;

          const result = staff.reduce((board, memberScore) => {
            const { member } = memberScore;

            if (requiredQualifications.length === 0) {
              board.list.push(memberScore);
              return board;
            }

            if (member.qualifications.length === 0) {
              board.excludes.push(memberScore);
              return board;
            }

            const eligible = member.qualifications.find((memberQualification) => {
              return requiredQualifications.find((qualification) =>
                memberQualification.name === qualification.name
              )
            });

            if (eligible) {
              board.list.push(memberScore);
            } else {
              board.excludes.push(memberScore);
            }

            return board;
          }, scoreboard);

          // console.log(`rank : `, rank);

          return result;
        },
      },

      {
        key: RuleKey.EXCLUDING_VACATION_MEMBERS,
        enable: true,
        name: '排除放假成員',
        description: '當天預約放假成員會直接排除',
        calcScore(params) {
          const { holidays, date } = params;
          const scoreboard = cloneDeep(params.scoreboard);

          // 符合日期假日
          const vaildHolidays = holidays.filter((holiday) => holiday.date === date);

          const staff = cloneDeep(scoreboard.list);
          scoreboard.list.length = 0;

          const result = staff.reduce((board, memberScore) => {
            const { member } = memberScore;
            const thatDay = vaildHolidays.some((day) => day.member.name === member.name);

            if (thatDay) {
              board.excludes.push(memberScore);
            } else {
              board.list.push(memberScore);
            }

            return board;
          }, scoreboard);

          return result;
        },
      },

      {
        key: RuleKey.EQUAL_NUMBER_OF_JOBS,
        enable: true,
        name: '平分工作數量',
        description: '比較人力之工作數量，盡可能平分工作',
        setting: {
          weightedScore: 1
        },
        calcScore(params) {
          const { weightedScore } = this.setting;
          const { vacancy, classSchedule } = params;
          const scoreboard = cloneDeep(params.scoreboard);

          const job = vacancy.job!;
          const staff = scoreboard.list.map(({ member }) => member);

          const { getWorkloads } = useStaffMemberWorkload({
            jobTypes: [],
            jobs: [job],
            staff,
          });

          const workloads = getWorkloads(classSchedule);

          // 取得工作數量種類，由高至低，因為越多工作的人得分越少
          const jobsQuantity = [
            ...workloads.reduce((set, { jobs }) => {
              const quantity = jobs?.[0]?.quantity;
              return set.add(quantity)
            }, new Set<number>())
          ].sort((a, b) => b - a);

          // 逐一累加得分
          scoreboard.list.forEach((memberScore) => {
            // 取得工作數量
            const quantity = workloads.find(({ member }) => {
              if (memberScore.member.name !== member.name) {
                return false;
              }

              return true;
            })?.jobs[0].quantity;
            if (!quantity) return;

            const index = jobsQuantity.indexOf(quantity);
            // 不存在
            if (index === -1) return;


            const score = index * weightedScore;
            memberScore.score += score;
          });

          // 依照得分排序
          scoreboard.list.sort((a, b) => b.score - a.score);

          return scoreboard;
        },
      },

      {
        key: RuleKey.TRY_TO_WORK_CONTINUOUSLY,
        enable: true,
        name: '工作盡量連續',
        description: '讓工作連續，不要花花班',
        setting: {
          weightedScore: 1,
        },
        calcScore(params) {
          const { weightedScore } = this.setting;
          const { vacancy, classSchedule, date } = params;
          const scoreboard = cloneDeep(params.scoreboard);

          // 取得前一天日行程
          const yesterday = dayjs(date).add(-1, 'd').format('YYYY/MM/DD');
          const yesterdayItinerary = classSchedule.itineraries.find((itinerary) =>
            itinerary.date === yesterday
          );
          if (!yesterdayItinerary) {
            return scoreboard;
          }

          // 找到所有同樣工作的人員
          const executors = yesterdayItinerary.vacancies.filter((yesterdayVacancy) => {
            if (!yesterdayVacancy.executor) {
              return false;
            }

            if (yesterdayVacancy.job?.name !== vacancy.job?.name) {
              return false;
            }

            if (yesterdayVacancy.jobType?.name !== vacancy.jobType?.name) {
              return false;
            }

            return true;
          }).map((vacancy) => {
            return vacancy.executor;
          }) as StaffMember[];

          // 將此些人員加分
          scoreboard.list.forEach((memberScore) => {
            const beTarget = executors.find(({ name }) => memberScore.member.name === name);
            if (!beTarget) {
              return;
            }

            memberScore.score += weightedScore;
          });

          // 依照得分排序
          scoreboard.list.sort((a, b) => b.score - a.score);

          return scoreboard;
        },
      },

      {
        key: RuleKey.CAN_NOT_WORK_MORE_THAN_DAYS,
        enable: true,
        name: '不可連續上班超過指定天數',
        description: '限制連續上班天數，超過者直接排除',
        setting: {
          maxDays: 5
        },
        calcScore(params) {
          const { maxDays } = this.setting;

          const { date, classSchedule } = params;
          const scoreboard = cloneDeep(params.scoreboard);
          scoreboard.list.length = 0;

          const membersScore = cloneDeep(params.scoreboard.list);

          // 依序判斷是否有人超過 X 天
          membersScore.forEach((memberScore) => {
            const { member } = memberScore;
            const match = hasWorkedContinuously(
              member, date, classSchedule.itineraries, maxDays
            );

            if (match) {
              scoreboard.excludes.push(memberScore);
              return;
            }

            scoreboard.list.push(memberScore);
          });

          return scoreboard;
        },
      },

      {
        key: RuleKey.TRY_TAKE_MORE_THAN_DAYS_ON_VACATION,
        enable: true,
        name: '放假盡量不要放超過指定天數',
        description: '以免其他人連續上太多天',
        setting: {
          weightedScore: 1,
          days: 2,
        },
        calcScore(params) {
          const { weightedScore, days } = this.setting;

          const { date, classSchedule } = params;
          const scoreboard = cloneDeep(params.scoreboard);

          // 依序評估是否加分
          scoreboard.list.forEach((memberScore) => {
            const match = hasContinuousVacation(memberScore.member, date, classSchedule.itineraries, days);
            if (!match) {
              return;
            }

            memberScore.score += weightedScore;
          });

          // 依照得分排序
          scoreboard.list.sort((a, b) => b.score - a.score);

          return scoreboard;
        },
      },

      {
        key: RuleKey.NO_DUPLICATION_WORK_ON_SHIFT_ON_DAY,
        enable: true,
        name: '同一天內不可重複上同一個班別工作',
        description: '若小明今天已經排入白班 A 工作，即使人力短缺，也不會再排小明參加另一個白班 A 工作',
        calcScore(params) {
          const { date, classSchedule, vacancy } = params;
          const scoreboard = cloneDeep(params.scoreboard);
          scoreboard.list.length = 0;

          const membersScore = cloneDeep(params.scoreboard.list);

          // 取得今天日行程
          const todayItinerary = classSchedule.itineraries.find((item) => item.date === date);
          if (!todayItinerary) {
            return cloneDeep(params.scoreboard);
          }

          membersScore.forEach((memberScore) => {
            const { vacancies } = todayItinerary;

            const isJoined = vacancies.some((todayVacancy) => {
              const comparedItem = {
                ...vacancy,
                executor: memberScore.member,
              }

              return isEqual(
                pick(comparedItem, ['job', 'jobType', 'executor']),
                pick(todayVacancy, ['job', 'jobType', 'executor'])
              );
            });

            if (isJoined) {
              scoreboard.excludes.push(memberScore);
              return;
            }

            scoreboard.list.push(memberScore);
          });

          return scoreboard;
        },
      },

      {
        key: RuleKey.UNSUITABLE_SUCCESSION_LIST,
        enable: true,
        name: '排除花花班',
        description: '指定排班時不可接續班別組合',
        setting: {
          list: [
            {
              before: {
                name: '大夜',
                codename: 'N',
                description: '',
              },
              after: {
                name: '小夜',
                codename: 'E',
                description: '',
              },
            },
            {
              before: {
                name: '小夜',
                codename: 'E',
                description: '',
              },
              after: {
                name: '白班',
                codename: 'D',
                description: '',
              },
            },
          ],
        },
        calcScore(params) {
          const { date, classSchedule, vacancy } = params;
          const { setting } = this;

          const scoreboard = cloneDeep(params.scoreboard);
          scoreboard.list.length = 0;

          const membersScore = cloneDeep(params.scoreboard.list);

          // 取得昨天日行程
          const yesterdayDate = dayjs(date).add(-1, 'd').format('YYYY/MM/DD')
          const yesterdayItinerary = classSchedule.itineraries.find((item) => item.date === yesterdayDate);
          if (!yesterdayItinerary) {
            return cloneDeep(params.scoreboard);
          }

          membersScore.forEach((memberScore) => {
            const { vacancies } = yesterdayItinerary;
            const { member } = memberScore;

            const memberVacancy = vacancies.find((vacancy) => vacancy.executor?.name === member.name);

            // 假設昨天未排工作，加入候選
            if (!memberVacancy) {
              scoreboard.list.push(memberScore);
              return;
            }

            // console.log(`memberVacancy : `, memberVacancy.jobType?.codename);
            // console.log(`vacancy : `, vacancy.jobType?.codename);

            // 成員工作是不是「不可接續班別組合」的起點（before）
            const isBefore = setting.list.some(({ before }) =>
              memberVacancy.jobType?.codename === before.codename
            );
            if (!isBefore) {
              scoreboard.list.push(memberScore);
              return;
            }

            // 今天工作是不是「不可接續班別組合」的終點（after）
            const isAfter = setting.list.some(({ after }) =>
              vacancy.jobType?.codename === after.codename
            );
            if (!isAfter) {
              scoreboard.list.push(memberScore);
              return;
            }

            // 以上條件不符合，表示目前條件為「不可接續班別組合」
            scoreboard.excludes.push(memberScore);
          });

          return scoreboard;
        },
      },

      /*{
        enable: true,
        name: '',
        description: '',
        weightedScore: 1,
        calcScore(params) {
          const weightedScore = this.weightedScore ?? 1;
          const { vacancy, classSchedule } = params;
          const scoreboard = cloneDeep(params.scoreboard);
    
    
          return scoreboard;
        },
      },// */
    ],
  }),
  actions: {
  },
})
