import { StaffMember, Job, JobType, Qualification, DayItinerary, ClassSchedule, StaffMemberHoliday } from '../types/main.type';
import { cloneDeep } from 'lodash-es';
import { defineStore } from 'pinia';

interface State {
  staff: StaffMember[],
  jobs: Job[],
  jobTypes: JobType[],
  qualifications: Qualification[],
  dayItineraries: DayItinerary[],
  classSchedules: ClassSchedule[],
  holidays: StaffMemberHoliday[],
}

export const useMainStore = defineStore('main', {
  state: (): State => ({
    staff: [],
    jobs: [
      {
        name: '工作',
        codename: '',
        description: '',
        requiredQualifications: [],
      },
    ],
    jobTypes: [
      {
        name: '白班',
        codename: 'D',
        description: '',
      },
      {
        name: '小夜',
        codename: 'E',
        description: '',
      },
      {
        name: '大夜',
        codename: 'N',
        description: '',
      },
      {
        name: '12',
        codename: '12',
        description: '8點-20點/20點-8點',
      },
    ],
    qualifications: [
      {
        name: '護理師',
        description: ''
      },
      {
        name: '照服員',
        description: ''
      },
    ],
    dayItineraries: [],
    classSchedules: [],

    holidays: [],
  }),
  actions: {
    init() {
      const state = this.$state;

      Object.entries(state).forEach(([key, value]) => {
        const storeString = localStorage.getItem(`class-table-helper:${key}`);
        if (!storeString) return;

        if (['qualifications', 'rules'].includes(key)) {
          return;
        }

        this.$patch({
          [key]: JSON.parse(storeString)
        });
      });
    },

    addDayItinerary(dayItinerary: DayItinerary) {
      const state = this.$state;

      const clone = cloneDeep(dayItinerary);

      const existIndex = state.dayItineraries.findIndex((item) => {
        return item.date === dayItinerary.date;
      });

      if (existIndex >= 0) {
        this.$patch((state) => {
          state.dayItineraries.splice(existIndex, 1, clone);
        });
        return;
      }

      this.$patch((state) => {
        state.dayItineraries.push(clone);
      });
    },
    removeDayItinerary(date: string) {
      const state = this.$state;

      const existIndex = state.dayItineraries.findIndex((item) => {
        return item.date === date;
      });

      if (existIndex < 0) return;

      this.$patch((state) => {
        state.dayItineraries.splice(existIndex, 1);
      });
    },

    setStaff(staff) {
      this.$patch({
        staff
      });
    },

    addHoliday(holiday: StaffMemberHoliday) {
      const state = this.$state;

      const existingItem = state.holidays.find((item) => {
        if (item.date !== holiday.date) {
          return false;
        }

        return item.member.name === holiday.member.name;
      });

      if (existingItem) {
        return;
      }

      this.$patch((state) => {
        state.holidays.push(cloneDeep(holiday));
      });
    },
    removeHoliday(holiday: StaffMemberHoliday) {
      const state = this.$state;

      const existIndex = state.holidays.findIndex((item) => {
        if (item.date !== holiday.date) {
          return false;
        }

        return item.member.name === holiday.member.name;
      });

      if (existIndex < 0) return;

      this.$patch((state) => {
        state.holidays.splice(existIndex, 1);
      });
    },

    addClassSchedule(classSchedule: ClassSchedule) {
      const state = this.$state;

      const existingItem = state.classSchedules.find((item) => {
        if (item.name !== classSchedule.name) {
          return false;
        }

        return item.name === classSchedule.name;
      });

      if (existingItem) {
        return;
      }

      this.$patch((state) => {
        state.classSchedules.push(cloneDeep(classSchedule));
      });
    },
    removeClassSchedule(classSchedule: ClassSchedule) {
      const state = this.$state;

      const existIndex = state.classSchedules.findIndex((item) => {
        if (item.name !== classSchedule.name) {
          return false;
        }

        return item.name === classSchedule.name;
      });

      if (existIndex < 0) return;

      this.$patch((state) => {
        state.classSchedules.splice(existIndex, 1);
      });
    },
  },
})
