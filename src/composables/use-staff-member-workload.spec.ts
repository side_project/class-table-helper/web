import { jobsEq, jobTypesEq } from '@/common/utils';
import { useStaffMemberWorkload } from '@/composables/use-staff-member-workload';
import { ClassSchedule } from '@/types/main.type';

describe('example', () => {
  const staff = [
    {
      enable: true,
      name: '人員A',
      codename: 'A',
      description: '',
      qualifications: [],
      jobTypes: [],
    },
    {
      enable: true,
      name: '人員B',
      codename: 'B',
      description: '',
      qualifications: [],
      jobTypes: [],
    },
    {
      enable: false,
      name: '人員C',
      codename: 'C',
      description: '',
      qualifications: [],
      jobTypes: [],
    },
  ];
  const jobs = [
    {
      name: '工作1',
      codename: '1',
      description: '',
      requiredQualifications: [],
    },
    {
      name: '工作2',
      codename: '2',
      description: '',
      requiredQualifications: [],
    }
  ];
  const jobTypes = [
    {
      name: '白班',
      codename: '白',
      description: '',
    },
    {
      name: '晚班',
      codename: '晚',
      description: '',
    }
  ];

  const { getWorkloads } = useStaffMemberWorkload({
    staff,
    jobs,
    jobTypes
  });

  it('單一成員、1 種工作、1 種班別', () => {
    const member = staff[0];
    const job = jobs[0];
    const jobType = jobTypes[0];

    const classSchedule: ClassSchedule = {
      name: '班表',
      description: '',
      month: 'YYYY/MM',
      itineraries: [
        {
          date: 'YYYY/MM/DD',
          vacancies: [
            {
              executor: member,
              job,
              jobType,
              requiredQualifications: [],
              remark: '',
            }
          ],
        }
      ],
    }

    const workloads = getWorkloads(classSchedule);

    const memberWorkload = workloads.find((workload) =>
      workload.member.name === member.name
    );

    const jobWorkload = memberWorkload?.jobs.find(({ item }) =>
      jobsEq(item, job)
    );
    const jobTypeWorkload = memberWorkload?.jobTypes.find(({ item }) =>
      jobTypesEq(item, jobType)
    );

    expect(memberWorkload).not.toBeUndefined();

    expect(jobWorkload).not.toBeUndefined();
    expect(jobWorkload?.quantity).toEqual(1);

    expect(jobTypeWorkload).not.toBeUndefined();
    expect(jobTypeWorkload?.quantity).toEqual(1);
  });

  it('單一成員、1 種工作、2 種班別', () => {
    const member = staff[0];
    const job = jobs[0];
    const jobType01 = jobTypes[0];
    const jobType02 = jobTypes[1];

    const classSchedule: ClassSchedule = {
      name: '班表',
      description: '',
      month: 'YYYY/MM',
      itineraries: [
        {
          date: 'YYYY/MM/DD',
          vacancies: [
            {
              executor: member,
              job,
              jobType: jobType01,
              requiredQualifications: [],
              remark: '',
            },
            {
              executor: member,
              job,
              jobType: jobType02,
              requiredQualifications: [],
              remark: '',
            },
          ],
        }
      ],
    }

    const workloads = getWorkloads(classSchedule);

    const memberWorkload = workloads.find((workload) =>
      workload.member.name === member.name
    );

    const jobWorkload = memberWorkload?.jobs.find(({ item }) =>
      jobsEq(item, job)
    );
    const jobTypeWorkload = memberWorkload?.jobTypes.find(({ item }) =>
      jobTypesEq(item, jobType01)
    );

    expect(memberWorkload).not.toBeUndefined();

    expect(jobWorkload).not.toBeUndefined();
    expect(jobWorkload?.quantity).toEqual(2);

    expect(jobTypeWorkload).not.toBeUndefined();
    expect(jobTypeWorkload?.quantity).toEqual(1);
  });
});
