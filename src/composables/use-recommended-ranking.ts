import { StaffMember, Vacancy, ClassSchedule, StaffMemberHoliday, Rank, StaffMemberScore, Scoreboard } from "../types/main.type";
import { Rule } from "../types/rule.type";
import { cloneDeep } from 'lodash-es';
import { useStaffMemberWorkload } from "./use-staff-member-workload";

interface GetScoreParams {
  /** 候選人力 */
  staff: StaffMember[];
  /** 目標職缺 */
  targetVacancy: Vacancy;
  /** 目標排班日期 */
  targetDate: string;
  /** 目前班表 */
  classSchedule: ClassSchedule;
  /** 使用規則 */
  rules: Rule[];
  /** 假日 */
  holidays: StaffMemberHoliday[];
}

export function useRecommendedRanking() {
  /** 建立排名資料 */
  const createRank = (staff: StaffMember[]) => {
    return {
      order: cloneDeep(staff),
      exclude: [],
    } as Rank
  }

  /** 建立記分板 */
  const createScoreboard = (staff: StaffMember[]) => {
    const list: StaffMemberScore[] = staff.map((member) => ({
      member,
      score: 0,
    }));

    return {
      list,
      excludes: [],
    } as Scoreboard;
  }

  const getScore = (params: GetScoreParams) => {
    const { staff, targetVacancy, targetDate, classSchedule, rules, holidays } = params;

    const scoreboard = rules.reduce((scoreboard, rule) => {
      if (!rule.enable) {
        return scoreboard;
      }

      return rule.calcScore({
        scoreboard,
        vacancy: targetVacancy,
        date: targetDate,
        classSchedule,
        holidays,
      });
    }, createScoreboard(staff));

    return scoreboard;
  }

  return {
    getScore,
  }
}