import { setActivePinia, createPinia } from 'pinia'
import { useRecommendedRanking } from '@/composables/use-recommended-ranking';
import { JobType, StaffMember, Vacancy, ClassSchedule } from '@/types/main.type';
import { useRuleStore } from '@/store/rule.store';
import { Rule, RuleCanNotWorkMoreThanDays, RuleUnsuitableSuccessionList } from '@/types/rule.type';
import { clone, find, propEq } from 'ramda';


const findRule = (name: string, rules: Rule[]): Rule | undefined => find(propEq('name', name), rules);
const ruleStore = useRuleStore(createPinia());

describe('限制員工班別', () => {
  const { getScore } = useRecommendedRanking();

  const rule = findRule('限制員工班別', ruleStore.rules);
  if (!rule) {
    throw new Error(`查無規則`);
  }

  it('沒人符合班別', () => {
    const jobType: JobType = {
      name: '白班',
      codename: '白',
      description: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '不符合之人',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [jobType],
      }
    ]

    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '小夜',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/01',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    expect(scoreboard.list.length).toEqual(0);
    expect(scoreboard.excludes.length).toEqual(1);
  });

  it('1 人符合班別', () => {
    const jobType: JobType = {
      name: '白班',
      codename: '白',
      description: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '符合之人',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [jobType],
      }
    ]

    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType,
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/01',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    expect(scoreboard.list.length).toEqual(1);
    expect(scoreboard.excludes.length).toEqual(0);
  });

  it('1 人符合、1 人不符合班別', () => {
    const jobType: JobType = {
      name: '白班',
      codename: '白',
      description: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '符合之人',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [jobType],
      },
      {
        enable: true,
        name: '不符合之人',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [{
          name: '小夜',
          codename: '白',
          description: '',
        }],
      },
    ]

    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType,
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/01',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    expect(scoreboard.list.length).toEqual(1);
    expect(scoreboard.excludes.length).toEqual(1);
  });
});

describe('需要擁有工作資格', () => {
  const { getScore } = useRecommendedRanking();

  const rule01 = findRule('需要擁有工作資格', ruleStore.rules);
  if (!rule01) {
    throw new Error(`查無規則`);
  }

  // const classSchedule: ClassSchedule = {
  //   name: '',
  //   description: '',
  //   month: '2021/12',
  //   itineraries: [],
  // }

  it('沒人', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }

    const scoreboard = getScore({
      staff: [],
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule01],
      holidays: [],
    });

    expect(scoreboard.list.length).toEqual(0);
    expect(scoreboard.excludes.length).toEqual(0);
  });

  it('工作無任何資格限制', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '符合之人',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '符合之人',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule01],
      holidays: [],
    });

    expect(scoreboard.list.length).toEqual(2);
    expect(scoreboard.excludes.length).toEqual(0);
  });

  it('員工無任何資格', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [
        {
          name: '資格 1',
          description: '',
        }
      ],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '符合之人',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '符合之人',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule01],
      holidays: [],
    });

    expect(scoreboard.list.length).toEqual(0);
    expect(scoreboard.excludes.length).toEqual(2);
  });

  it('1 人符合「職缺」資格', () => {
    const qualification01 = {
      name: '資格 1',
      description: '',
    }

    const qualification02 = {
      name: '資格 2',
      description: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '符合之人',
        codename: '',
        description: '',
        qualifications: [
          qualification01
        ],
        jobTypes: [],
      }
    ]

    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [
        ],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [qualification01, qualification02],
      executor: null,
      remark: '',
    }

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule01],
      holidays: [],
    });

    expect(scoreboard.list.length).toEqual(1);
    expect(scoreboard.excludes.length).toEqual(0);
  });

  it('1 人符合「工作」資格', () => {
    const qualification01 = {
      name: '資格 1',
      description: '',
    }

    const qualification02 = {
      name: '資格 2',
      description: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '符合之人',
        codename: '',
        description: '',
        qualifications: [
          qualification01
        ],
        jobTypes: [],
      }
    ]

    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [
          qualification01, qualification02
        ],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule01],
      holidays: [],
    });

    expect(scoreboard.list.length).toEqual(1);
    expect(scoreboard.excludes.length).toEqual(0);
  });

  it('1 人不符合「工作」資格', () => {
    const qualification01 = {
      name: '資格 1',
      description: '',
    }

    const qualification02 = {
      name: '資格 2',
      description: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '符合之人',
        codename: '',
        description: '',
        qualifications: [
          qualification01
        ],
        jobTypes: [],
      }
    ]

    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [
          qualification02
        ],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule01], holidays: [],
    });

    expect(scoreboard.list.length).toEqual(0);
    expect(scoreboard.excludes.length).toEqual(1);
  });

  it('1 人不符合、1 人符合「工作」資格', () => {
    const qualification01 = {
      name: '資格 1',
      description: '',
    }

    const qualification02 = {
      name: '資格 2',
      description: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '符合之人',
        codename: '',
        description: '',
        qualifications: [
          qualification01
        ],
        jobTypes: [],
      },
      {
        enable: true,
        name: '不符合之人',
        codename: '',
        description: '',
        qualifications: [
          qualification02
        ],
        jobTypes: [],
      },
    ]

    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [
          qualification01
        ],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule01], holidays: [],
    });

    expect(scoreboard.list.length).toEqual(1);
    expect(scoreboard.list[0].member.name).toEqual('符合之人');

    expect(scoreboard.excludes.length).toEqual(1);
    expect(scoreboard.excludes[0].member.name).toEqual('不符合之人');
  });

  it('1 人不符合、3 人符合「工作」資格', () => {
    const qualification01 = {
      name: '資格 1',
      description: '',
    }

    const qualification02 = {
      name: '資格 2',
      description: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '符合之人1',
        codename: '',
        description: '',
        qualifications: [
          qualification01
        ],
        jobTypes: [],
      },
      {
        enable: true,
        name: '符合之人2',
        codename: '',
        description: '',
        qualifications: [
          qualification01
        ],
        jobTypes: [],
      },
      {
        enable: true,
        name: '符合之人3',
        codename: '',
        description: '',
        qualifications: [
          qualification01
        ],
        jobTypes: [],
      },
      {
        enable: true,
        name: '不符合之人',
        codename: '',
        description: '',
        qualifications: [
          qualification02
        ],
        jobTypes: [],
      },
    ]

    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [
          qualification01
        ],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule01],
      holidays: [],
    });

    expect(scoreboard.list.length).toEqual(3);

    expect(scoreboard.excludes.length).toEqual(1);
    expect(scoreboard.excludes[0].member.name).toEqual('不符合之人');
  });
});

describe('排除放假成員', () => {
  const rule = findRule('排除放假成員', ruleStore.rules);
  if (!rule) {
    throw new Error(`查無規則`);
  }

  const { getScore } = useRecommendedRanking();

  it('指定放假日與工作日同一天', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '人員2',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }
    const date = '2021/12/01';

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: date,
      classSchedule,
      rules: [rule],
      holidays: [
        {
          date,
          member: staff[0],
        }
      ],
    });

    expect(scoreboard.excludes.length).toEqual(1);
    expect(scoreboard.excludes[0].member.name).toEqual(staff[0].name);

    expect(scoreboard.list.length).toEqual(1);
    expect(scoreboard.list[0].member.name).toEqual(staff[1].name);
  });

  it('指定放假日與工作日不同天', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '人員2',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }
    const date = '2021/12/01';

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: date,
      classSchedule,
      rules: [rule],
      holidays: [
        {
          date: '2021/12/02',
          member: staff[0],
        }
      ],
    });

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list.length).toEqual(2);
  });

  it('所有人都放假', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '人員2',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }
    const date = '2021/12/01';

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: date,
      classSchedule,
      rules: [rule],
      holidays: [
        {
          date,
          member: staff[0],
        },
        {
          date,
          member: staff[1],
        }
      ],
    });

    expect(scoreboard.excludes.length).toEqual(2);
    expect(scoreboard.list.length).toEqual(0);
  });
});

describe('平分工作數量', () => {
  const rule = findRule('平分工作數量', ruleStore.rules);
  if (!rule) {
    throw new Error(`查無規則`);
  }

  const { getScore } = useRecommendedRanking();

  it('1 人有工作、1 人無工作', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '人員2',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        }
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list[0].member.name).toEqual(staff[1].name);
  });

  it('2 人有不同天同一種工作、1 人無工作', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '人員2',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '人員3',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/02',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[1],
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list[0].member.name).toEqual(staff[2].name);
  });

  it('2 人有同一天同一種工作、1 人無工作', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '人員2',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '人員3',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[1],
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list[0].member.name).toEqual(staff[2].name);
  });

  it('1 人工作 1、1 人工作 2，要求推薦工作 1', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '人員2',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
            {
              job: {
                name: '工作2',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[1],
              remark: '',
            },
          ],
        }
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list[0].member.name).toEqual(staff[1].name);
  });

  it('A 三個工作、B 一個工作、C 無工作', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: 'A',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: 'B',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: 'C',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/02',
          vacancies: [
            {
              job: {
                name: '工作',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/03',
          vacancies: [
            {
              job: {
                name: '工作',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/04',
          vacancies: [
            {
              job: {
                name: '工作',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[1],
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list[0].member.name).toEqual(staff[2].name);
  });

  it('A 兩個工作、B 一個工作、C 一個工作、D 無工作', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: 'A',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: 'B',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: 'C',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: 'D',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/02',
          vacancies: [
            {
              job: {
                name: '工作',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/03',
          vacancies: [
            {
              job: {
                name: '工作',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[1],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/04',
          vacancies: [
            {
              job: {
                name: '工作',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[2],
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/23',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list[1].score).toEqual(scoreboard.list[2].score);
    expect(scoreboard.list[0].member.name).toEqual(staff[3].name);
  });
});

describe('工作盡量連續', () => {
  const rule = findRule('工作盡量連續', ruleStore.rules);
  if (!rule) {
    throw new Error(`查無規則`);
  }

  const { getScore } = useRecommendedRanking();

  it('1 人前一天有相同工作、1 人無工作', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '人員2',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        }
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/02',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list[0].member.name).toEqual(staff[0].name);
  });

  it('2 人前一天有相同工作', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '人員2',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[1],
              remark: '',
            },
          ],
        }
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/02',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
  });

  it('2 人前一天無相同工作', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
      {
        enable: true,
        name: '人員2',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[1],
              remark: '',
            },
          ],
        }
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/03',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list[0].score).toEqual(0);
    expect(scoreboard.list[1].score).toEqual(0);
  });
});

describe('不可連續上班超過指定天數', () => {
  const targetRule = findRule('不可連續上班超過指定天數', ruleStore.rules);
  if (!targetRule) {
    throw new Error(`查無規則`);
  }

  const rule = clone(targetRule) as RuleCanNotWorkMoreThanDays;
  rule.setting.maxDays = 6;

  const { getScore } = useRecommendedRanking();

  it('連續上 5 天', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/02',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/03',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/04',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/05',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/06',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list.length).toEqual(1);
  });

  it('連續上 6 天', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/02',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/03',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/04',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/05',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/06',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/07',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(1);
    expect(scoreboard.list.length).toEqual(0);
  });

  it('連續上 6 天後隔一天', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/02',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/03',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/04',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/05',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
        {
          date: '2021/12/06',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/08',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list.length).toEqual(1);
  });
});

describe('同一天內不可重複上同一個班別工作', () => {
  const targetRule = findRule('同一天內不可重複上同一個班別工作', ruleStore.rules);
  if (!targetRule) {
    throw new Error(`查無規則`);
  }

  const rule = clone(targetRule);

  const { getScore } = useRecommendedRanking();

  it('已上白班', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: staff[0],
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/01',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(1);
    expect(scoreboard.list.length).toEqual(0);
  });

  it('沒有上班', () => {
    const vacancy: Vacancy = {
      job: {
        name: '工作1',
        codename: '1',
        description: '',
        requiredQualifications: [],
      },
      jobType: {
        name: '白班',
        codename: '白',
        description: '',
      },
      requiredQualifications: [],
      executor: null,
      remark: '',
    }

    const staff: StaffMember[] = [
      {
        enable: true,
        name: '人員1',
        codename: '',
        description: '',
        qualifications: [],
        jobTypes: [],
      },
    ]

    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: '白',
                description: '',
              },
              requiredQualifications: [],
              executor: null,
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff,
      targetVacancy: vacancy,
      targetDate: '2021/12/01',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list.length).toEqual(1);
  });
});

describe('排除花花班（N 不接 E、E 不接 D）', () => {
  const targetRule = findRule('排除花花班', ruleStore.rules);
  if (!targetRule) {
    throw new Error(`查無規則`);
  }

  const rule = clone(targetRule) as RuleUnsuitableSuccessionList;
  rule.setting.list = [
    {
      before: {
        name: '大夜',
        codename: 'N',
        description: '',
      },
      after: {
        name: '小夜',
        codename: 'E',
        description: '',
      },
    },
    {
      before: {
        name: '小夜',
        codename: 'E',
        description: '',
      },
      after: {
        name: '白班',
        codename: 'D',
        description: '',
      },
    },
  ]

  const { getScore } = useRecommendedRanking();

  const vacancyN: Vacancy = {
    job: {
      name: '大夜',
      codename: '大夜',
      description: '',
      requiredQualifications: [],
    },
    jobType: {
      name: '大夜',
      codename: 'N',
      description: '',
    },
    requiredQualifications: [],
    executor: null,
    remark: '',
  }

  const vacancyE: Vacancy = {
    job: {
      name: '小夜',
      codename: '小夜',
      description: '',
      requiredQualifications: [],
    },
    jobType: {
      name: '小夜',
      codename: 'E',
      description: '',
    },
    requiredQualifications: [],
    executor: null,
    remark: '',
  }

  const vacancyD: Vacancy = {
    job: {
      name: '白班',
      codename: '白班',
      description: '',
      requiredQualifications: [],
    },
    jobType: {
      name: '白班',
      codename: 'D',
      description: '',
    },
    requiredQualifications: [],
    executor: null,
    remark: '',
  }

  const member: StaffMember = {
    enable: true,
    name: '人員1',
    codename: '',
    description: '',
    qualifications: [],
    jobTypes: [],
  };

  it('昨天無工作，排名不變', () => {
    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [],
    }

    const scoreboard = getScore({
      staff: [clone(member)],
      targetVacancy: clone(vacancyN),
      targetDate: '2021/12/02',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list.length).toEqual(1);
  });

  it('N 可接 N', () => {
    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '大夜',
                codename: 'N',
                description: '',
              },
              requiredQualifications: [],
              executor: clone(member),
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff: [clone(member)],
      targetVacancy: clone(vacancyN),
      targetDate: '2021/12/02',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list.length).toEqual(1);
  });

  it('D 可接 D', () => {
    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '白班',
                codename: 'D',
                description: '',
              },
              requiredQualifications: [],
              executor: clone(member),
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff: [clone(member)],
      targetVacancy: clone(vacancyD),
      targetDate: '2021/12/02',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list.length).toEqual(1);
  });

  it('N 不接 E', () => {
    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '大夜',
                codename: 'N',
                description: '',
              },
              requiredQualifications: [],
              executor: clone(member),
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff: [clone(member)],
      targetVacancy: clone(vacancyE),
      targetDate: '2021/12/02',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(1);
    expect(scoreboard.list.length).toEqual(0);
  });

  it('E 不接 D', () => {
    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '小夜',
                codename: 'E',
                description: '',
              },
              requiredQualifications: [],
              executor: clone(member),
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff: [clone(member)],
      targetVacancy: clone(vacancyD),
      targetDate: '2021/12/02',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(1);
    expect(scoreboard.list.length).toEqual(0);
  });

  it('E 隔一天後可接 D', () => {
    const classSchedule: ClassSchedule = {
      name: '',
      description: '',
      month: '2021/12',
      itineraries: [
        {
          date: '2021/12/01',
          vacancies: [
            {
              job: {
                name: '工作1',
                codename: '1',
                description: '',
                requiredQualifications: [],
              },
              jobType: {
                name: '小夜',
                codename: 'E',
                description: '',
              },
              requiredQualifications: [],
              executor: clone(member),
              remark: '',
            },
          ],
        },
      ],
    }

    const scoreboard = getScore({
      staff: [clone(member)],
      targetVacancy: clone(vacancyD),
      targetDate: '2021/12/03',
      classSchedule,
      rules: [rule],
      holidays: [],
    });

    // console.log(`scoreboard : `, JSON.stringify(scoreboard, null, 4));

    expect(scoreboard.excludes.length).toEqual(0);
    expect(scoreboard.list.length).toEqual(1);
  });
});
