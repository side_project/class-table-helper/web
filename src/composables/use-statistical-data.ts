import { Vacancy, DayItinerary } from '../types/main.type';
import dayjs from 'dayjs';
import { filter, flow, groupBy, reduce, unionBy } from 'lodash';
import { computed, Ref, ref } from 'vue'

interface StatisticalDataParams {
  /** 職缺 */
  vacancies?: Ref<Vacancy[]>;
  /** 排班目標月份 */
  rosterMonth?: Ref<string>;
  /** 日行程 */
  dayItineraries?: Ref<DayItinerary[]>;
}

export function useStatisticalData(params: StatisticalDataParams) {
  const { vacancies = ref([]), rosterMonth = null, dayItineraries = ref([]) } = params;

  /** 統計指定的數值出現次數，並以 object 呈現 */
  const countKey = flow(
    (item, key) => {
      return groupBy(item, key);
    },
    (item) => {
      return reduce(
        item,
        (result, value, key) => {
          if (key !== 'undefined') {
            result[key] = value.length;
          }
          return result;
        },
        {}
      );
    }
  );

  /** 工作名稱與數量 */
  const numberOfJobMap = computed(() => countKey(vacancies.value, 'job.name'));

  /** 班別與數量 */
  const numberOfJobTypeMap = computed(() => countKey(vacancies.value, 'jobType.name'));

  /** 月份天數 */
  const monthDays = computed(() => {
    if (!rosterMonth) return 0;

    return dayjs(rosterMonth.value).daysInMonth();
  });

  /** 已排日行程天數 */
  const scheduledDays = computed(() => {
    if (!rosterMonth) return 0;

    return flow(
      (data) => {
        return filter(data, (datum) => {
          return (
            dayjs(datum.date).month() === dayjs(rosterMonth.value).month()
          );
        });
      },
      (data) => {
        return unionBy(data, 'date');
      },
      (data) => {
        return data.length;
      }
    )(dayItineraries.value)
  });

  /** 未排日行程天數 */
  const unscheduledDays = computed(() => monthDays.value - scheduledDays.value);

  return {
    numberOfJobMap,
    numberOfJobTypeMap,
    monthDays,
    scheduledDays,
    unscheduledDays,
  }
}
