import { StaffMember, Job, JobType, StaffMemberHoliday, Vacancy, ClassSchedule } from '../types/main.type';
import { Rule } from '../types/rule.type';
import { cloneDeep } from 'lodash';
import { useRecommendedRanking } from './use-recommended-ranking';
import { useStaffMemberWorkload } from './use-staff-member-workload';

interface UseParams {
  rules: Rule[];
  staff: StaffMember[];
  jobs: Job[];
  jobTypes: JobType[];
  holidays: StaffMemberHoliday[];
}

interface YieldExecutorResult {
  executor: StaffMember | null;
  targetVacancy: Vacancy;
  targetDate: string;
}

export function usePoster(params: UseParams) {
  const { rules, staff, jobs, jobTypes, holidays } = params;

  const { getScore } = useRecommendedRanking();

  /** 迭代班表、持續加入參與者 */
  const yieldExecutor = function* (schedule: ClassSchedule) {
    for (const dayItinerary of schedule.itineraries) {
      for (const vacancy of dayItinerary.vacancies) {
        // 根據目前班表取得推薦排序
        const scoreboard = getScore({
          staff,
          targetVacancy: vacancy,
          targetDate: dayItinerary.date,
          classSchedule: schedule,
          rules,
          holidays,
        });

        // 取第一名
        vacancy.executor = scoreboard.list?.[0]?.member ?? null;

        const result: YieldExecutorResult = {
          executor: vacancy.executor,
          targetVacancy: vacancy,
          targetDate: dayItinerary.date,
        }

        yield result;
      }
    }
  }

  /** 將人力排入已排定日程之班表 */
  const calcClassSchedule = (schedule: ClassSchedule, callback?: (arg: YieldExecutorResult) => void) => {
    const classSchedule = cloneDeep(schedule);

    // 將職缺依照日期排序
    classSchedule.itineraries.sort((a, b) => a.date > b.date ? 1 : -1);

    const iterator = yieldExecutor(classSchedule);

    while (true) {
      const result = iterator.next();

      if (result.value) {
        callback && callback(result.value);
      }

      if (result.done) {
        break;
      }
    }

    return classSchedule;
  }

  return {
    /** 將人力排入已排定日程之班表 */
    calcClassSchedule,
  }
}