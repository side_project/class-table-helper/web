import { jobsEq, jobTypesEq } from "../common/utils";
import { StaffMember, Job, JobType, StaffMemberWorkload, ClassSchedule } from "../types/main.type";

interface UseParams {
  staff: StaffMember[],
  jobs: Job[],
  jobTypes: JobType[],
}


export function useStaffMemberWorkload(useParams: UseParams) {
  const { staff, jobs, jobTypes } = useParams;

  /** 建立空白 workloads */
  const createEmptyWorkloads = () => {
    const workloads: StaffMemberWorkload[] = staff
      .filter((member) => member.enable)
      .map((member) => {
        const workloadJobs = jobs.map((job) => ({
          item: job,
          quantity: 0,
        }));

        const workloadJobTypes = jobTypes.map((jobType) => ({
          item: jobType,
          quantity: 0,
        }));

        return {
          member,
          jobs: workloadJobs,
          jobTypes: workloadJobTypes,
        }
      });

    return workloads;
  }


  const getWorkloads = (classSchedule: ClassSchedule) => {
    const workloads = createEmptyWorkloads();

    // 依序提取日行程
    classSchedule.itineraries.forEach((dayItinerary) => {
      // 依序提取職缺
      dayItinerary.vacancies.forEach((vacancy) => {
        // 比對 member
        const target = workloads.find((workload) =>
          workload.member.name === vacancy.executor?.name
        );
        if (!target) {
          return;
        }

        // 累加 Job
        if (vacancy.job) {
          const tJob = target.jobs.find(({ item }) =>
            jobsEq(item, vacancy.job as Job)
          );

          if (tJob) {
            tJob.quantity += 1;
          }
        }

        // 累加 JobType
        if (vacancy.jobType) {
          const tJobType = target.jobTypes.find(({ item }) =>
            jobTypesEq(item, vacancy.jobType as JobType)
          );

          if (tJobType) {
            tJobType.quantity += 1;
          }
        }
      });
    });

    return workloads;
  }

  return {
    getWorkloads,
  }
}